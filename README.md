# This SVG icon shows today's date

The background image is derived from the [Twitter TweMoji Calendar icon](https://github.com/twitter/twemoji/blob/gh-pages/2/svg/1f4c5.svg) - CC-BY.

## Use
SVG supports JavaScript. Browsers will only run the JavaScript if the SVG is included. You can past JS into SVG or init it from different file.

* in an iframe `<iframe src="calendar.svg"></iframe>`
* as inline `<body><svg>.....</svg></body>`

It will not run JavaScript in an `<img>` element.

## Demo
Public demo you can find on my [CodePen](https://codepen.io/therealnorman/details/PQXRZx/).

Enjoy!
